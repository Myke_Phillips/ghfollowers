//
//  GFButton.swift
//  GHFollowers
//
//  Created by Myke Phillips on 24/02/2020.
//  Copyright © 2020 Myke Phillips. All rights reserved.
//

import UIKit

class GFButton: UIButton {


    override init(frame: CGRect) {
        super.init(frame: frame)
        configure()
        
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    init(backgroundColour: UIColor, title: String) {
        super.init(frame: .zero)
        self.backgroundColor = backgroundColour
        self.setTitle(title, for: .normal)
        configure()
        
    }
    
    private func configure() {
        translatesAutoresizingMaskIntoConstraints = false
        layer.cornerRadius    = 10
        setTitleColor(.white, for: .normal)
        titleLabel?.font      = UIFont.preferredFont(forTextStyle: .headline)
    }
    
    

}
