//
//  User.swift
//  GHFollowers
//
//  Created by Myke Phillips on 25/02/2020.
//  Copyright © 2020 Myke Phillips. All rights reserved.
//

struct User {
    var login: String
    var avatarUrl: String
    var name: String?
    var location: String?
    var bio: String?
    var publicRepos: Int
    var publicGists: Int
    var htmlUrl: String
    var following: Int
    var followers: Int
    var reatedAt: String
    
}
