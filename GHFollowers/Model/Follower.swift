//
//  Follower.swift
//  GHFollowers
//
//  Created by Myke Phillips on 25/02/2020.
//  Copyright © 2020 Myke Phillips. All rights reserved.
//

struct Follower: Codable, Hashable {
    var login: String
    var avatarUrl: String
    
}
