//
//  SearchVC.swift
//  GHFollowers
//
//  Created by Myke Phillips on 24/02/2020.
//  Copyright © 2020 Myke Phillips. All rights reserved.
//

import UIKit

class SearchVC: UIViewController {
    
    let logoImageView      = UIImageView()
    let usernameTextFiled  = GFTextField()
    let callToActionButton = GFButton(backgroundColour: .systemGreen, title: "Get Followers")
    
    var isUsenameEntered: Bool { return !usernameTextFiled.text!.isEmpty }

    override func viewDidLoad() {
        super.viewDidLoad()    
        view.backgroundColor = .systemBackground
        configureLogoImageView()
        configureTextField()
        configureActionButton()
        createDissmissKeyboardtapGesture()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: true)

        
    }
    
    func createDissmissKeyboardtapGesture() {
        let tap = UITapGestureRecognizer(target: self.view, action: #selector(UIView.endEditing(_:)))
        
        view.addGestureRecognizer(tap)
    }
    
    @objc func pushFollowersListVC() {
        
        guard isUsenameEntered else {
            
            presentGFAlertOnMainThread(title: "Empty username", message: "Please enter a username. We need to know who to look for 😀.", buttontitle: "OK")
            return
            
        }
        
        
        
        usernameTextFiled.resignFirstResponder()
        let followerListVC = FollowerListVC()
        followerListVC.username = usernameTextFiled.text
        followerListVC.title = usernameTextFiled.text
        
        navigationController?.pushViewController(followerListVC, animated: true)
         
    }
    
    
    func configureLogoImageView() {
        view.addSubview(logoImageView)
        logoImageView.translatesAutoresizingMaskIntoConstraints = false
        logoImageView.image = UIImage(named: "gh-logo")
        
        NSLayoutConstraint.activate([
            logoImageView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 80),
            logoImageView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            logoImageView.heightAnchor.constraint(equalToConstant: 200),
            logoImageView.widthAnchor.constraint(equalToConstant: 200)
        
        
        ])
    }
    
    func configureTextField() {
        view.addSubview(usernameTextFiled)
        usernameTextFiled.delegate = self
        
        NSLayoutConstraint.activate([
            usernameTextFiled.topAnchor.constraint(equalTo: logoImageView.bottomAnchor, constant: 48),
            usernameTextFiled.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 50),
            usernameTextFiled.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -50),
            usernameTextFiled.heightAnchor.constraint(equalToConstant: 50),
        
        
        
        ])
    }
    
    func configureActionButton() {
        view.addSubview(callToActionButton)
        callToActionButton.addTarget(self, action: #selector(pushFollowersListVC), for: .touchUpInside)
        
        NSLayoutConstraint.activate([
            callToActionButton.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -50),
            callToActionButton.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 50),
            callToActionButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -50),
            callToActionButton.heightAnchor.constraint(equalToConstant: 50)
            
        
        ])
    }

}

extension SearchVC: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        pushFollowersListVC()
        
        return true
    }
}
