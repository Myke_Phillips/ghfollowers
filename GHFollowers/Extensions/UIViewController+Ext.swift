//
//  UIViewController+Ext.swift
//  GHFollowers
//
//  Created by Myke Phillips on 25/02/2020.
//  Copyright © 2020 Myke Phillips. All rights reserved.
//

import UIKit

fileprivate var containerView: UIView!

extension UIViewController {
    
    func presentGFAlertOnMainThread(title: String, message: String, buttontitle: String) {
        
        DispatchQueue.main.async {
            let alertVc                    = GFAlertVC(title: title, message: message, buttonTitle: buttontitle)
            alertVc.modalPresentationStyle = .overFullScreen
            alertVc.modalTransitionStyle   = .crossDissolve
            
            self.present(alertVc, animated: true)
            
            
        }
        
    }
    
    
    func showLoadingView() {
        containerView = UIView(frame: view.bounds)
        view.addSubview(containerView)
        
        containerView.backgroundColor = .systemBackground
        containerView.alpha = 0
        
        UIView.animate(withDuration: 0.25) { containerView.alpha = 0.8 }
    
        let activityIndicator = UIActivityIndicatorView(style: .large)
        containerView.addSubview(activityIndicator)
        
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            activityIndicator.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            activityIndicator.centerXAnchor.constraint(equalTo: view.centerXAnchor)
        
        ])
        
        activityIndicator.startAnimating()
        
        
    }
    
    func dismissLoadingView() {
        
        DispatchQueue.main.async {
            containerView.removeFromSuperview()
            containerView = nil
        }
    }
    
    func showEmptyStateView(with message: String, in view: UIView) {
        let emptyStateView = GFEmptyStateView(message: message)
        emptyStateView.frame = view.bounds
        view.addSubview(emptyStateView)
    }
}
